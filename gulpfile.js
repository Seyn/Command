var elixir = require('laravel-elixir');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
/*
elixir(function(mix) {
    mix.sass('app.scss');
});
*/
elixir(function(mix) {
    mix.scripts([
        "lib/jquery.min.js",
        "lib/underscore.js",
        "lib/backbone.js",
        "lib/bootstrap.min.js",
        "lib/html5shiv.min.js",
        "lib/respond.min.js",
        "lib/scroll.min.js",
        "models/executeModel.js",
        "models/Email.js",
        "collections/executeCollection.js",
        "functions/set-templates.js",
        "functions/terminal-row.js",
        "views/terminalBody.js",
        "views/terminalFormRowView.js",
        "views/terminalAnswer.js",
        "views/modal.js",
        "views/contact-form.js",
        "views/loading.js",
        "controllers/Command.js",
        "controllers/cmds/showMap.js",
        "controllers/cmds/showContact.js",
        "app.js",
    ]);
});

elixir(function(mix) {
    mix.styles([
        "lib/bootstrap.min.css",
        "lib/bootstrap-theme.min.css",
        "lib/font-awesome.min.css",
        "lib/font-awesome.min.css",
        "style.css"
    ], 'public/build/css/all.css');
});

gulp.task('minify', function() {
    return gulp.src('resources/views/home/index.blade.php')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('resources/views/home/test'))
});
