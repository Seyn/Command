var app = app || {};

$(function(){

    app.Execute = function(cmd) {
        app.CommandController.set(cmd);
    },

    app.Scripts = function() {
        $('.terminal').click(function(){
            $(this).find('input').focus();
        });
        $('.scrollbar-inner').scrollbar();
    }

    app.Scripts();

});


