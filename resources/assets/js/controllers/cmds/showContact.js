var app = app || {};

$(function(){

    app.CommandController.contactSended = false;
    app.CommandController.showContact = function(){
        if( app.CommandController.contactSended ){
            var execute = new app.ExecuteModel({ response: 'Hmm.. Que extraño.. tengo la sensación de que ya he vivido este momento.'});
            new app.TerminalAnswer({model: execute});

        } else {
            var contact =  new app.ContactForm({ el: $('#modal') });

            new app.Modal({ modal_title: 'Contacta', content: contact.$el.html() });
        }

    }

});