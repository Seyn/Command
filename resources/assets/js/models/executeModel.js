var app = app || {};

app.ExecuteModel = Backbone.Model.extend({
    defaults: {
        _token: $(settings.token).attr('content'),
        cmd: null,
        response: null
    },
    urlRoot: function() {
        return settings.base_url + '/execute';
    },
    parse: function(response) {

    }
});