var app = app || {};

app.Email = Backbone.Model.extend({
    defaults: {
        _token: $(settings.token).attr('content'),
        name: null,
        email: null,
        message: null
    },
    urlRoot: function() {
        return settings.base_url + '/sendEmail';
    },

    initialize: function() {

    }

});