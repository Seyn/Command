var app = app || {};

app.TerminalBody = Backbone.View.extend({
    tagName: 'div',
    className: 'terminal-body',
    el: $('.terminal-content'),

    template: _.template( $('#terminal-body').html() ),

    initialize: function(){
      this.render();
    },

    render: function(){
        this.$el.html( this.template );
        return this;
    }

});


new app.TerminalBody;