var app = app || {};

app.TerminalAnswer = Backbone.View.extend({
    tagName: 'p',
    el: $('.answer-container'),
    template: _.template( $('#terminal-answer-row-tpl').html() ),

    initialize: function(){
        this.form = new app.TerminalFormRowView();
        this.$machine = this.form.$machine;

        if( this.model.get('cmd') != null ){
            this.$prefix = this.$machine.html() + this.model.get('cmd');
        }


        this.form.$input.val("");

        this.render();
        $(".answer-container").scrollTop($(".answer-container")[0].scrollHeight);
    },

    render: function(){
        this.$el.append( this.template({ prefix: this.$prefix, model: this.model.toJSON() }) );
        return this;
    }
});

