var app = app || {};

app.Modal = Backbone.View.extend({
    tagName: 'div',
    el: $('#modal'),
    template: _.template( $('#modal-tpl').html() ),

    initialize: function(obj){
        this.title = this.$('.modal-title');
        this.body = this.$('.modal-body');

        this.modal_title = obj.modal_title;
        this.content = obj.content;
        this.on('onClose', this.onClose, this);
        this.createModalListening();

        this.render();
    },

    createModalListening: function() {
        var self = this;
        $(this.el).on('hidden.bs.modal', function () {
            self.trigger('onClose');
        });
    },

    onClose: function(){
        console.log('closing..');
    },

    render: function(){
        this.$el.html( this.template({ title: this.modal_title, content: this.content }) );
        this.$el.modal();
        return this;
    }
});
