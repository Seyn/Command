var app = app || {};

app.TerminalFormRowView = Backbone.View.extend({
    tagName: 'form',
    className: 'execute',
    template: _.template( $('#terminal-form-row-tpl').html() ),

    events: {
        'submit': 'send'
    },

    reset: function(){
        this.$input.val("");
    },

    send: function(e) {
        e.preventDefault();

        var self = this;

        var execute_model = new app.ExecuteModel({cmd: $(settings.terminal.input).val()});


        // guardamos el modelo
        execute_model.save().done(function(data) {


            if(typeof data.response == 'object')
                var response = "Ejecutando..";
            else
                var response = data.response;

            execute_model.set('response', response);
            var answer_view = new app.TerminalAnswer({model: execute_model});
            self.reset();


            if( data.response.frontcmd )
                app.Execute(data.response.frontcmd);




        });

    },

    initialize: function(){
        this.render();

        this.$input = this.$('.cmd-input');
        this.$machine = this.$('.machine');

        this.$input.focus();
    },

    render: function(){

        this.$el.html( this.template )
        return this;
    }

});


new app.TerminalFormRowView({ el: $('.execute-container') })