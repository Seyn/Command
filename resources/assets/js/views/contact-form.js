var app = app || {};

app.ContactForm = Backbone.View.extend({
    tagName: 'form',
    className: 'contact-form',
    el: $('.contact-form'),
    template: _.template( $('#contact-form').html() ),
    loading_container: '.loading_container',

    events: {
        'click input[type="submit"]': 'submit'
    },

    initialize: function(obj){
        this.$el = obj.el;
        this.render();
    },

    submit: function(){

        var self = this;
        this.name = this.$('#name').val();
        this.email = this.$('#email').val();
        this.message = this.$('#message').val();

        var email = new app.Email({
            name: this.name,
            email: this.email,
            message: this.message
        });


        this.loadingElement = new app.LoadingElement();
        this.loadingElement.setElement( this.$el.find(this.loading_container) ).render();


        email
            .save()
                .success(function(){
                    self.modal = new app.Modal({
                        modal_title: 'Mensaje enviado correctamente',
                        content: "Muchas gracias por tu contacto, ya le llamaremos.."
                    });
                })
                .fail(function(){
                    self.modal = new app.Modal({
                        modal_title: 'El mensaje no se ha enviado',
                        content: "Ha habido algún error y no se ha acabado de enviar el mensaje, vuelve a intentarlo en un rato."
                    });
                })
                .done(function(){
                    self.loadingElement.remove();
                    app.CommandController.contactSended = true;
                });



    },



    render: function(){
        this.$el.html( this.template );
        return this;
    }
});
