var app = app || {};

app.LoadingElement = Backbone.View.extend({
    tagName: 'span',
    className: 'loading floatleft',
    template: _.template( $('#loading_tpl').html() ),


    initialize: function(obj) {

    },

    render: function(){
        this.$el.html( this.template );

        return this;
    }
});
