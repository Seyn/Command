<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Desarrollador web backend y frontend. Sin más.">
    <link rel="icon" href="../../favicon.ico">
    <meta name="csrf_token" id="token" content="{{ csrf_token() }}" >
    <title>Albert's Web Page</title>
    <link rel="stylesheet" href="{{ URL::to('build/css/all.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,700' rel='stylesheet' type='text/css'>

    <script>
        var settings = {terminal: {
                el: '.terminal',
                body: '.terminal-body',
                row: '.terminal-row',
                row_tpl: '#terminal-row-tpl',
                input: '.cmd-input',
                form: '.execute'
            },
            base_url: 'http://albertramos.me',
            token: '#token'

        }
    </script>
</head>

<body>

<div class="container">

    <div class="header">
        <div class="row">

            <div class="col-md-12">
                <p></p>

            </div>

        </div>


        <div class="row">

            <div class="col-md-4">
                <div class="summary">
                    <div class="profile-img">
                        <img src="{{ URL::to('img/profile/mrx.png') }}" alt="Albert Ramos Pastor">
                    </div>

                    <div class="description">
                        <p class="nomarginbottom">
                            Bienvenidos,
                        </p>
                        <p>
                            mi nombre es Albert Ramos y soy un desarrollador web.
                        </p>
                        <p>
                            Dado que presentarme a mi mismo no es mi punto más fuerte, os dejo la consola interactiva en caso de que queráis saber más de mí.
                        </p>
                    </div>
                </div>

            </div>


            <div class="col-md-8">

                <div class="terminal">

                    <div class="terminal-header">
                        <p>albert cli [version 0.1.35]</p>
                        <p><i class="fa fa-copyright fa-rotate-180"></i> No rights reserved.</p>
                    </div>



                    <div class="terminal-content">



                    </div>


                </div>

            </div>

        </div>
    </div>

</div>


<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"></div>

<script type="text/template" id="modal-tpl">
    <!-- Modal -->

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="mdalLabel"><%= title %></h4>
                </div>
                <div class="modal-body">

                    <%= content %>

                </div>
            </div>
        </div>

</script>


<script type="text/template" id="contact-form">

        <div class="contact-form">
            <div class="form-group">
                <input type="text" class="form-control" id="name" placeholder="Nombre">
            </div>

            <div class="form-group">
                <input type="email" class="form-control" id="email" placeholder="Correo electrónico">
            </div>

            <div class="form-group">
                <textarea class="form-control" name="text" id="message" placeholder="Comentarios"></textarea>
            </div>

            <div class="form-group text-left loading_container floatleft">

            </div>

            <div class="form-group text-right">
                <input type="submit" class="btn btn-default" value="Enviar" />
            </div>
        </div>

</script>


<script type="text/template" id="terminal-body">
    <div class="terminal-body">
        <div class="terminal-row">

            <div class="answer-container">
            </div>

            <div class="execute-container clearfix">

            </div>

        </div>
    </div>
</script>




<script type="text/template" id="terminal-answer-row-tpl">
    <% if( prefix != null ) { %>
        <%= prefix %>
    <% } %>
    <div><%= model.response %></div>
</script>




<script type="text/template" id="terminal-form-row-tpl">
    <form class="execute">

            <div class="machine">
                <span>machine@awesome:/home$ </span>
            </div>

            <div class="input-container">
                <input type="text" name="cmd-input" class="cmd-input" autocomplete="off">
            </div>

        </div>
    </form>
</script>

<script type="text/template" id="loading_tpl">
    <i class="fa fa-cog fa-spin fa-2x "></i>
</script>
<script src="js/all.js"></script>
</body>
</html>

