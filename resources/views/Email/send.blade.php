<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <style>
            li {
                list-style: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">

                <ul>
                    <li>
                        Nombre: {{ $user->name }}
                    </li>
                    <li>
                        Email: {{ $user->email }}
                    </li>
                    <li>
                        Mensaje: <br>
                        {{ $user->message }}
                    </li>
                </ul>

            </div>
        </div>
    </body>
</html>
