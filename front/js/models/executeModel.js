var app = app || {};

app.ExecuteModel = Backbone.Model.extend({
   urlRoot: '/execute',
    defaults: {
        cmd: ''
    },
    constructor: function(data){
        this.cmd = data.cmd;

        console.log(this.cmd);
    }
});