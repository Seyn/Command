$(function(){

    $(settings.terminal.form).submit(function(e){
        e.preventDefault();

        var execute = new app.ExecuteModel({cmd: $(settings.terminal.input).val() });
        execute.save();
    });

});