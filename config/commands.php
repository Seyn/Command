<?php

return [

    'back' => [
        'getname' => 'Vaya, parece que no estás muy atento, me llamo Albert',


        'gethobbies' => 'En mi tiempo libre me encanta escuchar música, leer sobre nuevas tecnologías, ir a patinar ..',

        'getskills' => 'El lenguaje que más domino es PHP
                        conjunto con Laravel, el cual está hecha ésta api.
                        También se me da muy bien y me apasiona Javascript,
                        aun que puede llegar a ser un desastre fácilmente.',

        'getskills -php' => 'A parte de Laravel, también me gustan las formas de Symfony,
                              aun que no lo domine tanto.
                              Dejando de un lado los frameworks,
                              domino el desarrollo de plugins y temas de WordPress.
                              Aun que a veces me parece demasiado desordenado.
                              Por ello me gusta más desarrollar modulos de PrestaShop.',

        'getskills -js' => 'Los frameworks que domino de Javascript son jQuery y Backbone + Marionette.
                            Y me encanta la arquitectura NodeJS. Tengo pendiente jugar con él y ExpressJS.',

        'why' => 'Esta idea viene de que creo que no solo hay que decir lo que se te da bien,
                    también se ha de demostrar de forma que la mejor manera que se me ocurrió
                    para demostrar mis conocimientos era crear una algo parecido a esto.
                    Tened piedad, esto no es perfecto. Empezó como un experimento,
                    el cual debido a que cada vez la aplicación va creciendo y no tenía la estructura
                    perfectamente definida voy refactorizando. Poco a poco se va actualizando y añadiendo más funciones.
                    No solo se limita a mostrar información sobre mi, quiero que esto se convierta lo más interactivo
                    que pueda lograr e ir mejorandolo día a día.
                    Si tenéis alguna inquietud que necesitéis hacerme saber, podéis contactar conmigo sin ningún problema.'



    ],

    'front' => [

    ],

    'words' => [

    ]

];

/*
 * cmd: joke
 *
 * val: Esto son dos españoles...
 *
 * Desc: chiste
 *
 * Input:
 * Cuentame algun chiste;
 *
 * Busqueda de keywords, chiste dará true, y se mostrará dicho registro.
 *
 * Posibles inputs:
 *
 *
 * Cuentame un chiste
 * Cuentame algo sobre ti
 *
 * Mierda... en desarrollo.
 *
 */