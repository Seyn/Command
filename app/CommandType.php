<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommandType extends Model
{
    protected $table = 'command_types';

}
