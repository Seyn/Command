<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{

    public function scopeWhereLike( $query, $word ){
        return $query->where('word', 'LIKE', '%'.$word.'%');
    }
}
