<?php

namespace App\Http\Controllers\Api;

use App\Command;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommandsController extends Controller
{
    protected $cmd;

    public function __construct($cmd){
        $this->cmd = $cmd;
    }

    public function get(){
        $command = Command::where('command', $this->cmd)->first();

        if ( ! $command )
            return false;


        if( $command->type->name == "back")
            return $this->back();
        else
            return $this->front();
    }


    /**
     * Buscamos información sobre este comando
     *
     * @return bool|string
     */
    public function back(){

        $return_data = "";

        try {
            $data = Command::getCommand($this->cmd)->type('back')->first();

            if( ! $data )
                return false;

            $return_data = $data->value;

        } catch(Exception $e){

            return "Algo acaba de pasar por aquí, y no se muy bien el qué..";
        }


        return $return_data;
    }

    public function front() {
        $data = Command::getCommand($this->cmd)->type('front')->first();

        return ['frontcmd' => $data->value];

    }
}
