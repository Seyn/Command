<?php namespace App\Http\Controllers\Api;

use App\Command;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\CommandsController;
use Symfony\Component\Console\Helper\Helper;

class ReaderController extends Controller {

    protected $cmd;

    public function __construct($cmd){
        $this->cmd = $cmd;
    }

    /**
     * Examinamos contenido para ubicar la respuesta
     *
     * @return bool|string
     */
    public function exam(){

        $command = new CommandsController($this->cmd);

        if( $this->isValid() ) {

            // Si no existe información acerca de ese comando
            if( ! $command->get() )
                return "Me temo que esto no lo he aprendido aun.";
            else
                return $command->get();

        } else {

            return "¿A qué vienen estas confianzas?";
        }


    }

    public function isValid() {

        foreach( HelperController::makeArrayWords( $this->cmd ) as $word ) {

            $word_obj = new WordController( $word );

            // Si existe y no es valido
            if( $word_obj->exists() )
                if( ! $word_obj->isValid() )
                    return false;

        }

        return true;
    }
}