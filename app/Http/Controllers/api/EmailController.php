<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Response;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmailController extends Controller
{
    public function send(Request $req) {

        \Mail::send('Email.send', ['user' => $req] ,function ($m) use ($req) {
            $m->from($req->email, $req->name);

            $m->to('seyn@outlook.com', 'Seyn')->subject('Web');
        });

        if( count( Mail::failures() ) > 0 )
            return Response::HTTP_INTERNAL_SERVER_ERROR;

        return Response::json([], 201);
    }
}
