<?php

namespace App\Http\Controllers\api;

use App\Word;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WordController extends Controller
{
    protected $word;
    protected $word_obj;

    public function __construct($word) {
        $this->word = $word;
    }

    /**
     * Comprobamos si la palabra existe
     *
     * Si existe, miramos es una palabra permitida
     *
     * @return bool
     */
    public function isValid(){

        if( $this->word_obj )
            if( $this->word_obj->allowed )
                return true;

        return false;
    }


    /**
     * Comprobamos si la palabra existe
     *
     * @return bool
     */
    public function exists(){
        $this->word_obj = Word::whereLike($this->word)->first();

        if( ! $this->word_obj )
            return false;

        return true;
    }
}
