<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HelperController extends Controller
{
    static public function makeArrayWords( $sentence ){
        return explode(" ", $sentence);
    }
}
