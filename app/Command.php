<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    public function scopeGetCommand( $query, $cmd ){
        return $query->where('command', $cmd);
    }

    public function scopeType( $query, $type = 'back'){
        $type = CommandType::where('name', $type)->first();
        return $query->where('command_type_id', $type->id);
    }

    public function type(){
        return $this->belongsTo('App\CommandType', 'command_type_id');
    }

    public function scopeWhereLike( $query, $word ){
        return $query->where('word', 'LIKE', '%'.$word.'%');
    }
}
